import pyscreenshot as snip
import pyautogui as pag

import time
import numpy as np

def click(x,y):
  pag.click(x,y)

def move(x,y):
  pag.move(x,y)


def takeshot():
  return snip.grab()

def joinround():
  click(862,270+103)

def reizpass():
  click(589,473+103)

def reizgo():
  click(996,468+103)

def pressbeenden():
  click(1500,161)#58)

def pressok():
  click(840,591)

def pressaltok():
  click(792,561)

# def pressok2():
  # click(791,565)

def pressok2():
  click(850,500)



def getpixel(im,x,y):
  return im.getpixel((x,y))

def pixels(*qq):
  r=[]
  im=takeshot()
  for q in qq:
    r.append(getpixel(im,q[0],q[1]))
  return r

def pixeldelta(x1,x2):
  return np.sum(np.abs([y1-y2 for y1,y2 in zip(x1,x2)]))

def samecol(x1,x2):
  print("pixeldelta",x1,x2)
  return pixeldelta(x1,x2)<5
  
def shouldok():
  px=pixels((467,282),(409,310))
  ex1=[92,92,92]
  ex2=[47,25,14]
  return samecol(px[0],ex1) and samecol(px[1],ex2)

def shouldaltok():
  px=pixels((640,337),(613,345))
  ex1=[88,88,88]
  ex2=[78,48,34]
  return samecol(px[0],ex1) and samecol(px[1],ex2)

def presscard(i):
  if i==0:click(410,677)
  if i==1:click(492,677)
  if i==2:click(574,677)
  if i==3:click(651,677)
  if i==4:click(730,677)
  if i==5:click(814,677)
  if i==6:click(894,677)
  if i==7:click(969,677)
  if i==8:click(1045,677)
  if i==9:click(1136,677)

def movernd():
  pp=pag.position()
  x,y=pp.x,pp.y
  x+=np.random.randint(-3,3)
  y+=np.random.randint(-3,3)
  move(x,y)
  

def presscards():
  iss=[i for i in range(10)]
  np.random.shuffle(iss)
  for i in iss:
    presscard(i)
    # time.sleep(0.5)
    # movernd()
    # presscard(i)
    # time.sleep(1)
    # presscard(i)
    # time.sleep(0.5)


def shouldreiz():
  px=pixels((806,510),(778,467))
  ex1=[45,49,46]
  ex2=[101,62,38]
  return samecol(px[0],ex1) and samecol(px[1],ex2)

def iswaiting():
  px=pixels((468,375))[0]
  ex=[225,177,18]
  return samecol(px,ex)


# joinround()

# time.sleep(1)

# takeshot().save(str(np.random.randint(1000,10000))+".png")

# exit()

joinround()

while iswaiting():
  time.sleep(1)


while True:

  if shouldreiz():
    reizpass()
    continue
  
  # if shouldok():
    # pressok()
    # time.sleep(1)
    # pressok2()
    # time.sleep(1)
    # pressbeenden()
    # break
    
  # if shouldaltok():
    # pressaltok()
    # time.sleep(1)
    # pressok2()
    # time.sleep(1)
    # pressbeenden()
    # break
  
  exit()
  
  presscards()
    

  time.sleep(1)
  
  
  
